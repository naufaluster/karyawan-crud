package com.mandiri.karyawan.config.exception;

public class NotFoundException extends BaseCommonException {

    public NotFoundException(int code, String description) {
        this.code = code;
        this.description = description;
    }
}

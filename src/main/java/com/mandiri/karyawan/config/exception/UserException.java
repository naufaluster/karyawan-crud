package com.mandiri.karyawan.config.exception;

public class UserException extends BaseCommonException {

    public UserException(int code, String description) {
        this.code = code;
        this.description = description;
    }
}
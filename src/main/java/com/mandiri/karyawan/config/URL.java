package com.mandiri.karyawan.config;

public class URL {

    // POSITION
    public static final String POSITION_GET_LIST_ALL = "position/list/all";

    // EMPLOYEE
    public static final String EMPLOYEE_GET_LIST = "employee/list";
    public static final String EMPLOYEE_GET_COUNT_LIST = "employee/count";
}

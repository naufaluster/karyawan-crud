package com.mandiri.karyawan.controller;

import com.mandiri.karyawan.config.CommonResponse;
import com.mandiri.karyawan.config.URL;
import com.mandiri.karyawan.config.exception.NotFoundException;
import com.mandiri.karyawan.dao.service.EmployeeService;
import com.mandiri.karyawan.dao.service.PositionService;
import com.mandiri.karyawan.model.Employee;
import com.mandiri.karyawan.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private PositionService positionService;

    // Get list all position
    @RequestMapping(path = URL.POSITION_GET_LIST_ALL, method = RequestMethod.GET)
    public CommonResponse<List<Position>> getPositionList() throws NotFoundException {
        List<Position> position = positionService.getList();
        return new CommonResponse<>(200, "Success", position);
    }

    // Get Employee
    @RequestMapping(path = URL.EMPLOYEE_GET_LIST, method = RequestMethod.GET)
    public CommonResponse<List<Employee>> getEmployeeList(
            @RequestParam(name = "name", required = false, defaultValue = "") String name,
            @RequestParam(name = "idNumber", required = false, defaultValue = "") String idNumber,
            @RequestParam(name = "limit", defaultValue = "10") int limit,
            @RequestParam(name = "offset", defaultValue = "0") int offset,
            @RequestParam(name = "orderBy", defaultValue = "id") String orderBy,
            @RequestParam(name = "sortType", defaultValue = "ASC") String sortType) throws NotFoundException {
        List<Employee> employee = employeeService.getList(name, idNumber, limit, offset, orderBy, sortType);
        return new CommonResponse<>(200, "Success", employee);
    }

    @RequestMapping(path = URL.EMPLOYEE_GET_COUNT_LIST, method = RequestMethod.GET)
    public CommonResponse<Long> getEmployeeListCount(
            @RequestParam(name = "name", required = false, defaultValue = "") String name,
            @RequestParam(name = "idNumber", required = false, defaultValue = "") String idNumber,
            @RequestParam(name = "limit", defaultValue = "10") int limit,
            @RequestParam(name = "offset", defaultValue = "0") int offset) throws NotFoundException {
        Long count = employeeService.getCount(name, idNumber, limit, offset);
        return new CommonResponse<>(200, "Success", count);
    }
}

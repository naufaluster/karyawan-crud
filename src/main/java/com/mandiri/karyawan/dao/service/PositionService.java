package com.mandiri.karyawan.dao.service;

import com.mandiri.karyawan.config.exception.NotFoundException;
import com.mandiri.karyawan.dao.PositionDao;
import com.mandiri.karyawan.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class PositionService implements PositionDao {

    @Autowired
    private EntityManager session;

    @Override
    public List<Position> getList() throws NotFoundException {
        String hql = "FROM Position";
        Query query = session.createQuery(hql);
        List<Position> list = query.getResultList();
        if (list.size() < 1){
            throw new NotFoundException(404, "Position data does not exist");
        } else {
            return list;
        }
    }
}

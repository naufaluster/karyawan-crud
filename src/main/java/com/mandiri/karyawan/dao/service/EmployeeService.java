package com.mandiri.karyawan.dao.service;

import com.mandiri.karyawan.config.exception.NotFoundException;
import com.mandiri.karyawan.dao.EmployeeDao;
import com.mandiri.karyawan.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class EmployeeService implements EmployeeDao {

    @Autowired
    private EntityManager session;

    @Override
    public List<Employee> getList(String name, String idNumber, int limit, int offset, String orderBy, String sortType) throws NotFoundException {
        String order = getOrder(orderBy, sortType);
        String hql = "FROM Employee E WHERE E.name LIKE :name AND str(E.idNumber) LIKE :idNumber " + order;
        Query query = session.createQuery(hql)
                .setParameter("name", '%' + name + '%')
                .setParameter("idNumber", idNumber + '%')
                .setFirstResult(offset).setMaxResults(limit);
        List<Employee> list = query.getResultList();
        if (list.size() < 1) {
            throw new NotFoundException(404, "Data is not found");
        }
        return list;
    }

    @Override
    public Long getCount(String name, String idNumber, int limit, int offset) {
        String hql = "SELECT COUNT(E.id) FROM Employee E WHERE E.name LIKE :name AND str(E.idNumber) LIKE :idNumber";
        Query query = session.createQuery(hql)
                .setParameter("name", '%' + name + '%')
                .setParameter("idNumber", idNumber + '%')
                .setFirstResult(offset).setMaxResults(limit);
        Long count = (Long) query.getSingleResult();
        return count;
    }

    private String getOrder(String orderBy, String sortType) {
        String order = "";
        if (!orderBy.isEmpty()) {
            if (orderBy.equalsIgnoreCase("name")) {
                order = "ORDER BY E.name " + sortType;
            } else if (orderBy.equalsIgnoreCase("id")) {
                order = "ORDER BY E.id " + sortType;
            } else if (orderBy.equalsIgnoreCase("birthDate")) {
                order = "ORDER BY E.birthDate " + sortType;
            } else if (orderBy.equalsIgnoreCase("gender")) {
                order = "ORDER BY E.gender " + sortType;
            } else if (orderBy.equalsIgnoreCase("position")) {
                order = "ORDER BY E.position " + sortType;
            } else if (orderBy.equalsIgnoreCase("idNumber")) {
                order = "ORDER BY E.idNumber " + sortType;
            }
        } else {
            order = "";
        }
        return order;
    }
}

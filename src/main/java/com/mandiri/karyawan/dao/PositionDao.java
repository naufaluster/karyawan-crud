package com.mandiri.karyawan.dao;

import com.mandiri.karyawan.config.exception.NotFoundException;
import com.mandiri.karyawan.model.Position;

import java.util.List;

public interface PositionDao {
    // Get Position List
    List<Position> getList() throws NotFoundException;
}

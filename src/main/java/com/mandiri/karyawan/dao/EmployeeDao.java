package com.mandiri.karyawan.dao;

import com.mandiri.karyawan.config.exception.NotFoundException;
import com.mandiri.karyawan.model.Employee;

import java.util.List;

public interface EmployeeDao {

    // Get Employee List
    List<Employee> getList(String name, String idNumber, int limit, int offset, String orderBy, String sortType) throws NotFoundException;

    // Count Employee list
    Long getCount(String name, String idNumber, int limit, int offset) throws NotFoundException;
}
